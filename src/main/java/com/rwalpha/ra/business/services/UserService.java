package com.rwalpha.ra.business.services;

import java.util.List;
import java.util.Set;

import com.rwalpha.ra.data.model.Role;
import com.rwalpha.ra.data.model.User;


public interface UserService {
	void save(User user);
	void save(Role role);
	void removeUserById(Integer id);
	void removeRoleById(Integer id);

	List<User> listUsers();
	List<Role> listRoles();
	Role findRoleById(Integer id);
	User findUserById(Integer id);

	void addRolesToUser(Integer userId, Integer... roleIds);
	
	String logDatabase(String... tableNames);
	List<Role> findRoleExcluding(Set<Integer> excludeRoles);
	
}
