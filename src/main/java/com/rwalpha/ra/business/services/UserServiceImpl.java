package com.rwalpha.ra.business.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rwalpha.ra.business.repositories.RoleRepository;
import com.rwalpha.ra.business.repositories.UserRepository;
import com.rwalpha.ra.data.model.Role;
import com.rwalpha.ra.data.model.User;

@Service
//@Scope(proxyMode = ScopedProxyMode.INTERFACES)
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;

	@Override
//	@Transactional
	public void save(User user) {
		userRepository.save(user);

	}

	@Override
//	@Transactional
	public void save(Role role) {
		roleRepository.save(role);

	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return userRepository.findAll();
	}

	@Override
	public List<Role> listRoles() {
		return roleRepository.findAll();
	}

	@Override
	public User findUserById(Integer id) {
		Optional<User> user = userRepository.findById(id);
		return user.isPresent() ? user.get() : null;

	}

	@Override
	public Role findRoleById(Integer id) {
		roleRepository.findById(id);
		Optional<Role> role = roleRepository.findById(id);
		return role.isPresent() ? role.get() : null;

	}

	@Override
	@Transactional
	public void removeUserById(Integer id) {
		User user = this.findUserById(id);
		user.getRoles().forEach(roleiter -> {
			roleiter.getUsers().remove(user);
		});
		userRepository.deleteById(id);

	}

	@Override
	@Transactional
	public void removeRoleById(Integer id) {
		Role role1 = this.findRoleById(id);
		role1.getUsers().forEach(useriter -> {
			useriter.getRoles().remove(role1);
		});
		roleRepository.deleteById(id);
	}

	@Override
	public String logDatabase(String... tableNames) {
		return userRepository.logDatabase(tableNames);

	}

	@Override
	@Transactional
	public void addRolesToUser(Integer userId, Integer... roleIds) {
		User user = this.findUserById(userId);
		
		List<Integer> rolesList = Arrays.asList(roleIds);
		List<Integer> userRolesList = new ArrayList<Integer>(user.getRoles().size());
		logger.debug("addRolesToUser size of roleIds " + String.valueOf(rolesList.size()));

		user.getRoles().forEach(role -> userRolesList.add(role.getId()));
		
		// For each selected role id if user roles contains no addition of the selected role
		Arrays.stream(roleIds).forEach(roleId -> {
			logger.debug("addRolesToUser " + String.valueOf(userId) + " " + String.valueOf(roleId));
			Role role = findRoleById(roleId);
			if (!user.getRoles().contains(role)) {
				role.addUser(user);
				logger.debug("addRolesToUser roles: " + String.valueOf(role.getUsers().size()));
				save(role);
			}
		});

		// For each user role if selected user roles not contains user role, remove role from user
		for (Integer roleId : userRolesList) {
			if (!rolesList.contains(roleId)) {
				Role role = findRoleById(roleId);
				role.removeUser(user);
				logger.debug("addRolesToUser roles: removed role " + String.valueOf(role.getUsers().size()));
				save(role);
			}
		}
		
		save(user);
	}

	@Override
	public List<Role> findRoleExcluding(Set<Integer> excludeRoles) {
		return roleRepository.findRoleExcluding(excludeRoles);
	}

}
