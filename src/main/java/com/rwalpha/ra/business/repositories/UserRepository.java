package com.rwalpha.ra.business.repositories;

import com.rwalpha.ra.data.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<User, Integer> {
}
