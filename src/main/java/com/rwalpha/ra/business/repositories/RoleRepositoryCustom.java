package com.rwalpha.ra.business.repositories;

import com.rwalpha.ra.data.model.Role;

import java.util.List;
import java.util.Set;

public interface RoleRepositoryCustom  {
	
    public List<Role> findRoleExcluding(Set<Integer> excludeRoles);
}
