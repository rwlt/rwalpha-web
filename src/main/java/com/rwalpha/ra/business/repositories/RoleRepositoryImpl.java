package com.rwalpha.ra.business.repositories;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rwalpha.ra.data.model.Role;


public class RoleRepositoryImpl implements RoleRepositoryCustom {

	private static final Logger logger = LoggerFactory.getLogger(RoleRepositoryImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
    public List<Role> findRoleExcluding(Set<Integer> excludeRoles) {
		
		logger.debug("findRoleExcluding " + String.valueOf(excludeRoles.size()));
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Role> query = cb.createQuery(Role.class);
        Root<Role> role = query.from(Role.class); 
 
        Predicate excludePredicate;
        List<Predicate> predicates = new ArrayList<>();
        for (Integer excludeRole : excludeRoles) {
        	excludePredicate = cb.notEqual(role.<Integer>get("id"), excludeRole);
            predicates.add(excludePredicate);
        }
        if (predicates.size() > 0) {
	        query.select(role)
	            .where(cb.and(predicates.toArray(new Predicate[predicates.size()])));
        } else {
	        query.select(role).orderBy(new ArrayList<Order>());
        }
 
        return entityManager.createQuery(query).getResultList();
    }

}
