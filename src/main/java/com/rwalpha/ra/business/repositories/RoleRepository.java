package com.rwalpha.ra.business.repositories;

import com.rwalpha.ra.data.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseRepository<Role, Integer>, RoleRepositoryCustom  {
}
