package com.rwalpha.ra.business.repositories;

import javax.persistence.EntityManager;
import org.hibernate.Session;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.rwalpha.ra.utils.JdbcUtils;


public class BaseRepositoryImpl<T, ID> extends SimpleJpaRepository<T, ID> implements BaseRepository<T, ID> {

	private final EntityManager entityManager;

	public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		// Keep the EntityManager around to used from the newly introduced methods.
		this.entityManager = entityManager;
	}

	@Override
	public EntityManager getEntityManager() {
		return entityManager;
	}

	@Override
	public String logDatabase(String... tableNames) {
		// DEBUG, dump our tables
		StringBuilder sb = new StringBuilder("");
		entityManager.unwrap(Session.class)
				.doWork(connection -> { sb.append(JdbcUtils.dumpTables(connection, tableNames)); });
		return sb.toString();
	}
}
