package com.rwalpha.ra.business.repositories;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface BaseRepository<T, ID> extends JpaRepository<T, ID> {
	public EntityManager getEntityManager();
	public String logDatabase(String... tableNames) ;

}
