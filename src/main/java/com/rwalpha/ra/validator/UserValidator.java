package com.rwalpha.ra.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.rwalpha.ra.data.model.User;

@Component
public class UserValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
		
		//User user = (User) target;
		//if(user.getId() <=0){
		//	errors.rejectValue("id", "negativeValue", new Object[]{"'id'"}, "id can't be negative");
		//}
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "firstname.required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "lastname.required");
		
	}

}
