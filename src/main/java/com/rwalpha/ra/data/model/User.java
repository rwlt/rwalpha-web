package com.rwalpha.ra.data.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "USERS")
public class User {

	public User() {
		super();
	};

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	private Integer id;

	@Column(name = "FIRST_NAME")
	//@NotBlank
	private String firstName;

	@Column(name = "LAST_NAME")
	@NotBlank
	private String lastName;

	private boolean isDisabled;
	private boolean isDeleted;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinTable(name = "USERS_ROLES", joinColumns = { @JoinColumn(name = "USER_ID")},	 
	           inverseJoinColumns = { @JoinColumn(name = "ROLE_ID") })
	private Set<Role> roles = new HashSet<Role>(0);

	@Transient
	private List<Role> selectRoles = new ArrayList<Role>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isIsDisabled() {
		return this.isDisabled;
	}

	public void setIsDisabled(boolean isDisabled) {
		this.isDisabled = isDisabled;
	}

	public boolean isIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Set<Role> getRoles() {
		return this.roles;
	}

	public void setUsers(Set<Role> roles) {
		this.roles = roles;
	}

	public String getUserName() {
		StringBuilder sb = new StringBuilder();
		sb.append(firstName);
		sb.append(" ");
		sb.append(lastName);
		return sb.toString();
	}

	public List<Role> getSelectRoles() {
		return selectRoles;
	}

	public void setSelectRoles(List<Role> selectRoles) {
		this.selectRoles = selectRoles;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", isDisabled=" + isDisabled
				+ ", isDeleted=" + isDeleted + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof User))
	        return false;
		User other = (User) obj;
		return id != null && id.equals(other.getId());
	}

	@Override
	public int hashCode() {
		// NOTE: Using single hash bucket - only use small collections to keep performance.
		return 31;
	}

	
}
