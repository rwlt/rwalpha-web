package com.rwalpha.ra.data.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROLES")
public class Role {

	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private String description;

	@ManyToMany(mappedBy = "roles", cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	private Set<User> users = new HashSet<User>(0);

	public Role() {
		super();
	}

	public Role(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public Role(Integer id, String name, String description, Set<User> users ) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.users = users;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	public void addUser(User user) {
	   users.add(user);
	   user.getRoles().add(this);
	}

	public boolean removeUser(User user) {
	   boolean result = users.remove((User)user);
	   if (result) {
	      result = user.getRoles().remove((Role)this);
	   }
	   return result;
	}
	

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Role))
	        return false;
		Role other = (Role) obj;
		return id != null && id.equals(other.getId());
	}

	@Override
	public int hashCode() {
		// NOTE: Using single hash bucket - only use small collections to keep performance.
		return 31;
	}

	
}
