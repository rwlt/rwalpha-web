package com.rwalpha.ra.controller;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.expression.Strings;

import com.rwalpha.ra.business.services.UserService;
import com.rwalpha.ra.data.model.Role;
import com.rwalpha.ra.data.model.User;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	public HomeController() {
		super();
	}

	@Autowired
	private UserService userService;
	 
	@Autowired
	@Qualifier("userValidator") 
	private Validator validator;
	 
	
	@InitBinder private void initBinder(WebDataBinder binder) {
		binder.addValidators(validator); 
	}
	 

	@ModelAttribute("allUsers")
	public List<User> populateUsers() {
		return this.userService.listUsers();
	}

	@ModelAttribute("allRoles")
	public List<Role> populateRoles() {
		return this.userService.listRoles();
	}

	@ModelAttribute("chooseRoles")
	public List<Role> populateChooseRoles(final User user) {
		return populateChooseableRoles(user);
	}

	@ModelAttribute("userRoles")
	public String[] populateUserRoles(final User user, Locale locale) {
		Strings str = new Strings(locale);
		Function<Set<Role>, String[]> fn = parameter -> {
			String[] strs = new String[parameter.size()];
			int index = 0;
			for (Role r : parameter) {
				strs[index++] = r.getName();
			}
			return strs;
		};
		String[] roleMsg = str.arrayPrepend(fn.apply(user.getRoles()), "user.role.");
		logger.debug("role list: " + String.join(", ", roleMsg));
		return fn.apply(user.getRoles());
	}
    
	@ModelAttribute("modalOpen")
	public boolean isModalOpen() {
		return false;
	}
	
	@RequestMapping(value = { "/", "/home" })
	public String showHome(final User user, Locale locale, final ModelMap model) {
		logger.debug("showHome  No. of select roles: " + String.valueOf(user.getSelectRoles().size()));
		logger.debug("showHome  No. of roles: " + String.valueOf(user.getRoles().size()));
		return "home";
	}

	@RequestMapping(value = "/home", params = { "save" }, method = RequestMethod.POST)
	public ModelAndView saveUser(@Valid final User user, final BindingResult bindingResult, final ModelMap model) {
		if (bindingResult.hasErrors()) {
			logger.info("Returning userSave page");
			model.addAttribute("modalOpen", true);
			return new ModelAndView("home", model);//"home";
		}

		logger.debug("saveUser  user: " + user.toString());
		logger.debug("saveUser  No. of select roles: " + String.valueOf(user.getSelectRoles().size()));
		logger.debug("saveUser  No. of roles: " + String.valueOf(user.getRoles().size()));
		logger.debug("saveUser before user save");
		this.userService.save(user);
		Function<List<Role>, Integer[]> fn = parameter -> {
			Integer[] strs = new Integer[parameter.size()];
			int index = 0;
			for (Role r : parameter) {
				strs[index++] = r.getId();
			}
			return strs;
		};

		this.userService.addRolesToUser(user.getId(), fn.apply(user.getSelectRoles()));
		logger.debug("saveUser  No. of  roles: " + String.valueOf(user.getRoles().size()));
		model.clear();
		logger.debug("Check it is here at userSave page");
		logger.info("Returning userSaveSuccess page"); 
		model.addAttribute("modalOpen", false);
		return new ModelAndView("redirect:/home", model);
	}

	@RequestMapping(value = "/home", params = { "createUser" }, method = RequestMethod.GET)
	public ModelAndView createUser(final ModelMap model, final HttpServletRequest req) {
		final Integer doCreate = Integer.valueOf(req.getParameter("createUser"));
		logger.debug("createUser doCreate: " + String.valueOf(doCreate));
		if (doCreate == 1) {
			model.addAttribute("modalOpen", true);
			model.addAttribute("user", new User()); // clear any user details on
		}
		return new ModelAndView("home", model);
	}

	@RequestMapping(value = "/home", params = { "editUser" }, method = RequestMethod.POST)
	public ModelAndView editUser(final ModelMap model, final HttpServletRequest req) {
		final Integer userId = Integer.valueOf(req.getParameter("user_id"));
		logger.debug("editUser  userID: " + String.valueOf(userId));
		User userFindById = userService.findUserById(userId);
		logger.debug("editUser  user: " + userFindById.toString());
		model.addAttribute("modalOpen", true);
		userFindById.getRoles().forEach(action -> userFindById.getSelectRoles().add(action));
		model.addAttribute("user", userFindById);
		return new ModelAndView("home", model);
	}
	
	@RequestMapping(value = "/home", params = { "addRole" }, method = RequestMethod.POST)
	public ModelAndView addRole(final User user, final BindingResult bindingResult, final ModelMap model,
			final HttpServletRequest req) {
		final Enumeration<String> selectedIds = req.getParameterNames();
		for (String s : Collections.list(selectedIds)) {
			logger.debug("addRow  No. of modelattr selectedRoles: " + s);
		}
		final Integer roleId = Integer.valueOf(req.getParameter("selectRole"));
		
		Role role = userService.findRoleById(roleId);
		user.getSelectRoles().add(role);

		model.addAttribute("chooseRoles", populateChooseableRoles(user));
		model.addAttribute("modalOpen", true);
		return new ModelAndView("home", model);
	}

	@RequestMapping(value = "/home", params = { "removeRole" }, method = RequestMethod.POST)
	public ModelAndView removeRow(final User user, final BindingResult bindingResult, final ModelMap model, 
			final HttpServletRequest req) {
		final Integer rowId = Integer.valueOf(req.getParameter("removeRole"));
		user.getSelectRoles().remove(rowId.intValue());

		model.addAttribute("chooseRoles", populateChooseableRoles(user));
		model.addAttribute("modalOpen", true);
		return new ModelAndView("home", model);
	}

	private List<Role> populateChooseableRoles(final User user) {
		Set<Integer> excludeRoleIds = user.getSelectRoles().stream()
				.map(action -> action.getId())
				.collect(Collectors.toSet());
		logger.debug("populateChooseableRoles excludeRoleIds size: " + String.valueOf(excludeRoleIds.size()));
		return userService.findRoleExcluding(excludeRoleIds);
	}


}
