package com.rwalpha.ra.data;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.rwalpha.ra.business.repositories.BaseRepositoryImpl;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(repositoryBaseClass = BaseRepositoryImpl.class,
                    basePackages = {"com.rwalpha.ra.business.repositories"})
@PropertySource("classpath:raweb.properties")
public class HibernateConfigTest {

	@Autowired
	Environment env;
	
	private static final Logger logger = LoggerFactory.getLogger(HibernateConfigTest.class);

	@Bean
	@Profile("test")
	public DataSource dataSourceTest() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		dataSource.setUrl(env.getProperty("jdbc.url"));// ;shutdown=false  ?createDatabaseIfNotExist=true
		
		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		logger.info("Bean create local container EntityManager test factory bean.");
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSourceTest());
		em.setPackagesToScan(new String[] { "com.rwalpha.ra.data.model" });
		em.setJpaDialect(new HibernateJpaDialect());
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}

	private Properties additionalProperties() {
		Properties properties = new Properties();

		properties.setProperty("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
		properties.setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver");
		properties.setProperty("hibernate.connection.url", env.getProperty("jdbc.url"));
		properties.setProperty("hibernate.connection.username", "sa");
		properties.setProperty("hibernate.connection.password", "");

		
		//properties.setProperty("hibernate.hbm2ddl.auto", "update");
		//properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
		properties.setProperty("hibernate.hbm2ddl.auto", "create");
		
		properties.setProperty("hibernate.show_sql", "false");
		properties.setProperty("hibernate.format_sql", "false");
		properties.setProperty("hibernate.globally_quoted_identifiers", "true");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

		properties.setProperty("hibernate.c3p0.min_size", "8");
		properties.setProperty("hibernate.c3p0.max_size", "62");
		properties.setProperty("hibernate.c3p0.timeout", "252");
		properties.setProperty("hibernate.c3p0.acquire_increment", "2");
		properties.setProperty("hibernate.c3p0.idle_test_period", "110");
		properties.setProperty("hibernate.c3p0.max_statements", "110");

		return properties;
	}

	@Bean
	public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
		logger.info("Bean create JpaTransaction test manager");
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);
		return transactionManager;
	}

	/*
	 * @Bean public static PersistenceExceptionTranslationPostProcessor
	 * exceptionTranslation() { return new
	 * PersistenceExceptionTranslationPostProcessor(); }
	 */
}
