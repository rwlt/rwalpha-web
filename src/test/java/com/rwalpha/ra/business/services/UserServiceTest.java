package com.rwalpha.ra.business.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.rwalpha.ra.business.SpringBusinessConfig;
import com.rwalpha.ra.data.HibernateConfigTest;
import com.rwalpha.ra.data.model.Role;
import com.rwalpha.ra.data.model.User;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { SpringBusinessConfig.class, HibernateConfigTest.class })
@ActiveProfiles("test")
class UserServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceTest.class);

	@Autowired
	UserService userService;

	static List<User> userList = new ArrayList<User>();
	static List<Role> roleList = new ArrayList<Role>(); 

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		userList.clear();
		for (int i = 0; i < 10; i++) {
			User user = new User();
			user.setFirstName("Test " + i);
			user.setLastName("Test " + i);
			userList.add(user);
		}
		roleList.clear();
		for (int i = 0; i < 10; i++) {
			Role role = new Role();
			role.setDescription("Test role " + i);
			role.setName("Role" + i); 
			role.addUser(userList.get(0));
			role.addUser(userList.get(5));
			roleList.add(role);
		}
	}

	@AfterEach
	void tearDown() throws Exception {
		/*if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}*/
	}

	@Test
	@DirtiesContext
	@Rollback(true)
	void addUsersToDatabaseTest() {

		for (User user : userList) {
			userService.save(user);
		}

		for (Role role : roleList) {
			userService.save(role);
		}
		assertEquals(10, userService.listUsers().size());
		assertEquals(10, userService.listRoles().size());
		userService.listRoles().forEach(role -> {
			assertEquals(2, role.getUsers().size());
		});
	}

	@Test
	@DirtiesContext
	void removeRoleFromDatabaseTest() {
		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		assertEquals(0, userService.listUsers().size());
		assertEquals(0, userService.listRoles().size());

		assertEquals(0, userList.get(1).getRoles().size());
		assertEquals(0, userList.get(3).getRoles().size());

		Role role = new Role();
		role.setDescription("Test role ");
		role.setName("Role1");
		role.addUser(userList.get(1));

		Role role2 = new Role();
		role2.setDescription("Test role 2 ");
		role2.setName("Role2");
		role2.addUser(userList.get(1));

		userService.save(role);
		userService.save(role2);

		User user1 = userService.findUserById(1);
		assertEquals("Test 1", user1.getFirstName());
		assertEquals(1, user1.getId());
		assertEquals(2, user1.getRoles().size());

		// User 1 is in Role1 and Role2
		user1.getRoles().forEach(arole -> {
			switch (arole.getId()) {
			case 1:
				assertEquals("Role1", arole.getName());
				break;
			case 2:
				assertEquals("Role2", arole.getName());
				break;
			}
		});

		Role role1 = userService.findRoleById(1);
		assertEquals(1, role1.getId());

		// Remove Role1
		userService.removeRoleById(role1.getId());

		// User 1 is still in DB with only Role2
		user1 = userService.findUserById(1);
		assertEquals(1, user1.getRoles().size());
	}

	@Test
	@DirtiesContext
	void removeUserFromDatabaseTest() {
		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		assertEquals(0, userService.listUsers().size());
		assertEquals(0, userService.listRoles().size());

		assertEquals(0, userList.get(1).getRoles().size());
		assertEquals(0, userList.get(3).getRoles().size());

		for (User user : userList) {
			userService.save(user);
		}
		for (Role role : roleList) {
			userService.save(role);
		}

		User user3 = userService.findUserById(2);
		assertEquals("Test 5", user3.getFirstName());
		assertEquals(2, user3.getId());
		assertEquals(10, user3.getRoles().size());

		// Remove user3
		userService.removeUserById(user3.getId());

		// User 2 is still in DB with only 100 Roles
		User user2 = userService.findUserById(1);
		assertEquals("Test 0", user2.getFirstName());
		assertEquals(1, user2.getId());
		assertEquals(10, user2.getRoles().size());
	}

	@Test
	@DirtiesContext
	void addRolesToUserTest() {
		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		assertEquals(0, userService.listUsers().size());
		assertEquals(0, userService.listRoles().size());

		assertEquals(0, userList.get(1).getRoles().size());
		assertEquals(0, userList.get(3).getRoles().size());

		for (User user : userList) {
			userService.save(user);
		}
		for (Role role : roleList) {
			userService.save(role);
		}

		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		User user4 = userService.findUserById(4);
		assertEquals("Test 2", user4.getFirstName());
		assertEquals(4, user4.getId());
		assertEquals(0, user4.getRoles().size());

		assertEquals(2, userService.findRoleById(2).getUsers().size());
		assertEquals(2, userService.findRoleById(4).getUsers().size());

		// Add roles to User user4
		Integer[] roleIds = new Integer[]{1,2,3,4,5};
		userService.addRolesToUser(user4.getId(), roleIds); 
		// User has roles added
		user4 = userService.findUserById(4);
		assertEquals("Test 2", user4.getFirstName());
		assertEquals(4, user4.getId());
		assertEquals(5, user4.getRoles().size());
		assertEquals(3, userService.findRoleById(2).getUsers().size());
		assertEquals(3, userService.findRoleById(4).getUsers().size());

		// Add new roles to User user4 - userservice will add and remove roles from the list selected
		// and the user current roles if any in roles relationship.
		Integer[] roleIds2 = new Integer[]{1,3,5,7,9};
		userService.addRolesToUser(user4.getId(), roleIds2);

		user4 = userService.findUserById(4);
		assertEquals("Test 2", user4.getFirstName());
		assertEquals(4, user4.getId());
		assertEquals(5, user4.getRoles().size());
		assertEquals(3, userService.findRoleById(7).getUsers().size());
		assertEquals(2, userService.findRoleById(2).getUsers().size());
		assertEquals(2, userService.findRoleById(4).getUsers().size());
		assertEquals(3, userService.findRoleById(1).getUsers().size());
				
	}

	@Test
	@DirtiesContext
	void rolesExcludingTest() {
		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		assertEquals(0, userService.listUsers().size());
		assertEquals(0, userService.listRoles().size());
		for (User user : userList) {
			userService.save(user);
		}
		for (Role role : roleList) {
			userService.save(role);
		}
		if (logger.isDebugEnabled()) {
			logger.debug(userService.logDatabase("USERS", "USERS_ROLES", "ROLES"));
		}
		List<Role> roleChoose = userService.findRoleExcluding(new HashSet<Integer>());
		assertEquals(10, roleChoose.size());

		Set<Integer> rolesExclude = new HashSet<Integer>();
		rolesExclude.add(3);
		rolesExclude.add(5);
		rolesExclude.add(7);
		rolesExclude.add(8);
		List<Role> roleChoose2 = userService.findRoleExcluding(rolesExclude);
		assertEquals(6, roleChoose2.size());

	}

}
