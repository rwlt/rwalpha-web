<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>com.rwalpha.ra</groupId>
  <artifactId>RAWeb</artifactId>
  <version>0.0.1-SNAPSHOT</version>
  <packaging>war</packaging>

  <name>RAWeb</name>
  <description>RAlpha Web Application</description>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<maven.compiler.target>1.8</maven.compiler.target>
		<maven.compiler.source>1.8</maven.compiler.source>
		<java.version>1.8</java.version>
        <org.springframework.version>5.2.2.RELEASE</org.springframework.version>
        <org.springframework.data.version>2.2.3.RELEASE</org.springframework.data.version>
        <org.thymeleaf.version>3.0.11.RELEASE</org.thymeleaf.version>
        <org.thymeleaf.extras-version>3.0.1.RELEASE</org.thymeleaf.extras-version>
        <hibernate.version>5.2.11.Final</hibernate.version>
        <hibernate.validator>5.4.1.Final</hibernate.validator>
        <c3p0.version>0.9.5.2</c3p0.version>
        <javax.servlet-api.version>[4.0.0,4.1.0)</javax.servlet-api.version>
        <org.slf4j.version>1.7.21</org.slf4j.version>
        <org.log4j.version>1.2.17</org.log4j.version>
		<cglib.version>2.2.2</cglib.version>
	    <hsqldb.version>[2.5.0,2.6.0)</hsqldb.version>
	    <liquibase.version>[3.8.0,3.9.0)</liquibase.version>
	    <tomcat-dbcp.version>9.0.0.M26</tomcat-dbcp.version>
	</properties>

	<dependencies>
		<!-- Database configure -->
		<dependency>
	        <groupId>org.hsqldb</groupId>
	        <artifactId>hsqldb</artifactId>
	        <version>${hsqldb.version}</version>
	    </dependency>
	    <dependency>
	        <groupId>org.liquibase</groupId>
	        <artifactId>liquibase-core</artifactId>
	        <version>${liquibase.version}</version>
	        <exclusions>
		      <exclusion>
		         <groupId>ch.qos.logback</groupId>
				 <artifactId>logback-classic</artifactId>
		      </exclusion>
		      <exclusion>
				 <groupId>org.slf4j</groupId>
				 <artifactId>slf4j-api</artifactId>
		      </exclusion>
		    </exclusions>
	    </dependency>
    
		<!-- Logging -->
 		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${org.slf4j.version}</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>jcl-over-slf4j</artifactId>
			<version>${org.slf4j.version}</version>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${org.slf4j.version}</version>
			<scope>runtime</scope>
		</dependency>

  	    <dependency>
	      <groupId>log4j</groupId>
	      <artifactId>log4j</artifactId>
	      <version>${org.log4j.version}</version>
	      <scope>runtime</scope>
	    </dependency>

		<!-- Spring core & mvc -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${org.springframework.version}</version>
			<exclusions>
                <!-- Exclude Commons Logging in favor of SLF4j -->
                <exclusion>
                    <groupId>commons-logging</groupId>
                    <artifactId>commons-logging</artifactId>
                </exclusion>
            </exclusions>
		</dependency>

		<dependency>
		    <groupId>org.springframework</groupId>
		    <artifactId>spring-orm</artifactId>
		    <version>${org.springframework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${org.springframework.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<version>${org.springframework.version}</version>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-jpa</artifactId>
			<version>${org.springframework.data.version}</version>
		</dependency>

        <!-- Hibernate Core -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
            <version>${hibernate.version}</version>
	        <exclusions>
		    </exclusions>
        </dependency>
 
        <!-- Hibernate-C3P0 Integration -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-c3p0</artifactId>
            <version>${hibernate.version}</version>
	        <exclusions>
		    </exclusions>
        </dependency>

<!-- 	    <dependency>
            <groupId>org.apache.tomcat</groupId>
            <artifactId>tomcat-dbcp</artifactId>
            <version>${tomcat-dbcp.version}</version>
        </dependency>
 -->         
        <!-- c3p0 -->
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>${c3p0.version}</version>
        </dependency>
 
        <!-- Hibernate Validator -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate.validator}</version>
	        <exclusions>
		    </exclusions>
        </dependency>
        
        <!--  Thymeleaf  -->
		<dependency>
		    <groupId>org.thymeleaf</groupId>
		    <artifactId>thymeleaf</artifactId>
		    <version>${org.thymeleaf.version}</version>
		</dependency>
		
		<dependency>
		    <groupId>org.thymeleaf</groupId>
		    <artifactId>thymeleaf-spring5</artifactId>
		    <version>${org.thymeleaf.version}</version>
		</dependency>
  
  		<dependency>
            <groupId>org.thymeleaf.extras</groupId>
            <artifactId>thymeleaf-extras-java8time</artifactId>
            <version>${org.thymeleaf.extras-version}</version>
        </dependency>
        
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>${javax.servlet-api.version}</version>
            <scope>provided</scope>
        </dependency>


		<!-- For JavaConfig -->
<!-- 		<dependency>
		    <groupId>cglib</groupId>
		    <artifactId>cglib</artifactId>
		    <version>2.2.2</version>
		</dependency>
 -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter</artifactId>
			<version>5.5.0</version>
			<scope>test</scope>
		</dependency>

	</dependencies>

	<build>
 	    <resources>
    
	      <resource>
	        <directory>src/main/resources</directory>
	      </resource>
	      <resource>
	        <directory>src/main/java</directory>
	        <includes>
	          <include>**/*.properties</include>
	          <include>**/*.xml</include>
	          <include>**/*.html</include>
	        </includes>
	      </resource>
	      
	    </resources>

		<finalName>RAWeb</finalName>
		<plugins>
<!-- 			<plugin>
	            <artifactId>maven-war-plugin</artifactId>
	            <version>2.4</version>
	            <configuration>
	                <failOnMissingWebXml>false</failOnMissingWebXml>    
	            </configuration>
	        </plugin>
 -->
 			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
				</configuration>
			</plugin>
			
			<!-- embedded Jetty server, for testing -->
 			<plugin>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>9.4.24.v20191120</version>
				<configuration>
					<scanIntervalSeconds>10</scanIntervalSeconds>
					<webApp>
						<contextPath>/RAWeb</contextPath>
					</webApp>
			        <scanTargetPatterns>
			          <scanTargetPattern>
			            <directory>src/main/webapp/WEB-INF/views</directory>
			            <includes>
			              <include>**/*.html</include>
			              <include>**/*.properties</include>
			            </includes>
			          </scanTargetPattern>
			          <scanTargetPattern>
			            <directory>src/main/webapp/css</directory>
			            <includes>
			              <include>**/*.css</include>
			            </includes>
			          </scanTargetPattern>
			          <scanTargetPattern>
			            <directory>src/main/webapp/js</directory>
			            <includes>
			              <include>**/*.js</include>
			            </includes>
			          </scanTargetPattern>
			        </scanTargetPatterns>
				</configuration>
     	        <dependencies>
		           <dependency>
		             <groupId>org.slf4j</groupId>
		             <artifactId>slf4j-log4j12</artifactId>
		             <version>${org.slf4j.version}</version>
		           </dependency>
		        </dependencies>
			</plugin>

	        <plugin>
		        <groupId>org.apache.maven.plugins</groupId>
		        <artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M3</version>
		    </plugin>
		    <plugin>
	            <groupId>org.liquibase</groupId>
	            <artifactId>liquibase-maven-plugin</artifactId>
	            <version>3.8.1</version>
	            <configuration>
	                <propertyFile>src/main/resources/liquibase/liquibase.properties</propertyFile>
	                <changeLogFile>src/main/resources/liquibase/master.xml</changeLogFile>
	            </configuration>
	        </plugin>
			
		</plugins>
	</build>


</project>